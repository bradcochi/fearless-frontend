import './App.css'
import { BrowserRouter, Route, Routes } from "react-router-dom"

import Nav from './Nav'
import AttendConferenceForm from './AttendConferenceForm';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import PresentationForm from './PresentationForm';
import MainPage from './MainPage';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <>
      <BrowserRouter>
      <Nav />
        <Routes>
            <Route index element={<MainPage />} />
            <Route path="locations">
                <Route path="new" element={<LocationForm />} />
            </Route>
            <Route path="attendees">
                <Route index element={<AttendeesList attendees= {props.attendees} />}></Route>
                <Route path="new" element={<AttendConferenceForm />}></Route>
            </Route>
            <Route path="conferences">
                <Route path="new" element={<ConferenceForm />}></Route>
            </Route>
            <Route path="presentations">
                <Route path="new" element={<PresentationForm />}></Route>
            </Route>
        </Routes>
      </BrowserRouter>
    </>
  );
}

export default App;
